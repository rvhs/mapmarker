**This is a demo app for marking various markers on a Android Map**

This project has Map and in that map you can mark various graphical markers and label then accordingly. 
The available markers are Circle, Arc, line.

---

## How To use

1. Select a shape from toolbar.
2. Click on setting icon to select the setting that you want to use for drawing the shape
3. When you want to draw a shape **long tap** and the shape will be create and the pressed location. 
4. Now enter the label for this shape and store it in the database. It will also be added on the map.
5. There is Floating Action button that allow you to toggle the list of markers. Click on any item on the list to zoom onto the shape marker
6. You can also click on the marker and the list will be scrolled to the corresponding item.

---

## Libraries used

Latest android libraries are use so as to make the app more robust and maintainable

1. Android Architecture components 
2. Android Room ORM used for database
3. RxJava and RxAndroid used for task managment
4. ButterKnife is used for View injection and View managment
5. Dagger 2 is used for Dependency Injection and helps in keeping Modular Code
6. Other Support library and UI libraries

---

## Overview of project

There is no special architeture involved in the codebase. 
I have kept things very modular and tried to break the whole project in different funtionalities. Here is the list of them

1. RoomDbHelper - This class is used for helping the Activity with all the db related tasks. It uses Room ORM provided by google to do the data operations. We also use Rx to make the calls asyncronous. The code of db is abstacted from main activity and logics is hidden in this class.
2. GoogleMapHelper - This class will take the map instance in the view and will perform operations on it. It will add markers handle clicks and taps on the map. This will be injected using dagger at runtime.
3. SelectionOptions - This class will hold the info about what is selected by the user currently. It will be used in main activity
4. Also there are Marker Models created which will extend to ShapeMarker.
5. Db Entities are created and will be used for Db operations.

---

## Challenges

1. Since working with maps was new to me i had to go though untis and Google maps api. Also what shapes can be drawn and the scale of drawing took time to undestand
2. Using Room is also not that easy. Some aspect are very different from Othe ORM's and it took some time.([Thanks to this lib i was able to see data in real time] https://github.com/amitshekhariitbhu/Android-Debug-Database)
3. Creating of Models and Entities, verification of the geogrphical significance also wasted some time. Drawing shapes have many formula and precision cant be compramised
4. Click events for the map and drawing them on screen various interactions also were a hassle at first. It took a lot of time and effort to get it working.

---

## Things to improve

1. Currently there is no delete button which shoul be added
2. Shapes should be allowed to be edited too
3. Currently we select default colors but different colors should be allowed
4. Shapes cannot be rotated currently it also should be allowed in future