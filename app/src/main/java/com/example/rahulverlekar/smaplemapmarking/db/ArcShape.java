package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by rahulverlekar on 25/03/18.
 */

@Entity(tableName = "arc_shape")
public class ArcShape {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "latitude")
    public Double latitude;

    @ColumnInfo(name = "longitude")
    public Double longitude;

    public double radius;

    public double width;

}
