package com.example.rahulverlekar.smaplemapmarking.observer;

import com.example.rahulverlekar.smaplemapmarking.markers.ArcShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.CircleShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.LineShapeMarker;

/**
 * Created by rahulverlekar on 26/03/18.
 */

public interface GetAllShapesObserver {

    void circleShapeRetrieved(CircleShapeMarker shape);

    void lineShapeRetrieved(LineShapeMarker shape);

    void arcShapeRetrieved(ArcShapeMarker shape);

}
