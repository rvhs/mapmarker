package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by rahulverlekar on 25/03/18.
 */

@Entity(tableName = "line_shape")
public class LineShape {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "src_latitude")
    public Double srcLatitude;

    @ColumnInfo(name = "src_longitude")
    public Double srcLongitude;

    @ColumnInfo(name = "dest_latitude")
    public Double destLatitude;

    @ColumnInfo(name = "dest_longitude")
    public Double destLongitude;

    public double width;

}
