package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by rahulverlekar on 25/03/18.
 */
@Dao
public interface ArcDoa {
    @Query("SELECT * FROM arc_shape")
    Flowable<List<ArcShape>> getAll();

    @Query("SELECT * FROM arc_shape WHERE uid IN (:ids)")
    LiveData<List<ArcShape>> loadAllByIds(int[] ids);

    @Insert
    List<Long> insertAll(ArcShape... shapes);

    @Delete
    void delete(ArcShape arcShape);
}
