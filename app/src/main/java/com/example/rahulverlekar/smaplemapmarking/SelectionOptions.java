package com.example.rahulverlekar.smaplemapmarking;

import com.example.rahulverlekar.smaplemapmarking.markers.ShapeMarkerType;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by rahulverlekar on 21/03/18.
 */

//This class will hold the current selection information of the selection pallet
public class SelectionOptions {
    private ShapeMarkerType selectedShape;
    private double radius;
    private double width;
    private double length;
    private LatLng source, destination;
    private String name;

    //Default options
    public SelectionOptions() {
        selectedShape = ShapeMarkerType.CIRCLE;
        radius = 10;
        width = 20;
        length = 100;
        source = null;
        destination = null;
    }

    public SelectionOptions(ShapeMarkerType selectedShape, double radius, double width, double length) {
        this.selectedShape = selectedShape;
        this.radius = radius;
        this.width = width;
        this.length = length;
    }

    public void setSelectedShape(ShapeMarkerType selectedShape) {
        this.selectedShape = selectedShape;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public ShapeMarkerType getSelectedShape() {
        return selectedShape;
    }

    public double getRadius() {
        return radius;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public LatLng getSource() {
        return source;
    }

    public void setSource(LatLng source) {
        this.source = source;
    }

    public LatLng getDestination() {
        return destination;
    }

    public void setDestination(LatLng destination) {
        this.destination = destination;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void clearToDefaultState() {
        source = null;
        destination = null;
    }
}
