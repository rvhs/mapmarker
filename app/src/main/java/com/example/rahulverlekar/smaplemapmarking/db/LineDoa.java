package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by rahulverlekar on 25/03/18.
 */
@Dao
public interface LineDoa {
    @Query("SELECT * FROM line_shape")
    Flowable<List<LineShape>> getAll();

    @Query("SELECT * FROM line_shape WHERE uid IN (:ids)")
    LiveData<List<LineShape>> loadAllByIds(int[] ids);

    @Insert
    List<Long> insertAll(LineShape... shapes);

    @Delete
    void delete(LineShape circleShape);
}
