package com.example.rahulverlekar.smaplemapmarking.helper;

import android.content.Context;

import com.example.rahulverlekar.smaplemapmarking.Constant;
import com.example.rahulverlekar.smaplemapmarking.db.AppDatabase;
import com.example.rahulverlekar.smaplemapmarking.db.ArcShape;
import com.example.rahulverlekar.smaplemapmarking.db.CircleShape;
import com.example.rahulverlekar.smaplemapmarking.db.LineShape;
import com.example.rahulverlekar.smaplemapmarking.db.Shape;
import com.example.rahulverlekar.smaplemapmarking.markers.ArcShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.CircleShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.LineShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.ShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.observer.GetAllShapesObserver;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by rahulverlekar on 26/03/18.
 */

public class RoomDbHelper {
    private Context context;
    private AppDatabase database;

    public RoomDbHelper(Context context, AppDatabase database) {
        this.context = context;
        this.database = database;
    }

    private int addCircleToDb(CircleShape circleShape, String name, String color) {
        Shape parentShape = new Shape();
        parentShape.type = Constant.CIRCLE_SHAPE;
        List<Long> ids = database.circleDoa().insertAll(circleShape);
        parentShape.color = color;
        parentShape.subid = (ids != null && ids.size() > 0) ? ids.get(0).intValue() : -1;
        parentShape.name = name==null || name.trim().length() == 0 ? "Circle" + parentShape.subid : name;
        List<Long> parentIds = database.shapeDoa().insertAll(parentShape);
        return (parentIds != null && parentIds.size() > 0) ? parentIds.get(0).intValue() : -1;
    }

    private int addArcToDb(ArcShape arcShape, String name, String color) {
        Shape parentShape = new Shape();
        parentShape.type = Constant.ARC_SHAPE;
        List<Long> ids = database.arcDoa().insertAll(arcShape);
        parentShape.color = color;
        parentShape.subid = (ids != null && ids.size() > 0) ? ids.get(0).intValue() : -1;
        parentShape.name = name==null || name.trim().length() == 0 ? "Arc" + parentShape.subid : name;
        List<Long> parentIds = database.shapeDoa().insertAll(parentShape);
        return (parentIds != null && parentIds.size() > 0) ? parentIds.get(0).intValue() : -1;
    }

    private int addLineToDb(LineShape lineShape, String name, String color) {
        Shape parentShape = new Shape();
        parentShape.type = Constant.LINE_SHAPE;
        List<Long> ids = database.lineDoa().insertAll(lineShape);
        parentShape.color = color;
        parentShape.subid = (ids != null && ids.size() > 0) ? ids.get(0).intValue() : -1;
        parentShape.name = name==null || name.trim().length() == 0 ? "Line" + parentShape.subid : name;
        List<Long> parentIds = database.shapeDoa().insertAll(parentShape);
        return (parentIds != null && parentIds.size() > 0) ? parentIds.get(0).intValue() : -1;
    }

    public void addShapeToDb(ShapeMarker marker) {
        switch (marker.getType()) {
            case ARC:
                ArcShapeMarker arcShapeMarker = (ArcShapeMarker) marker;
                marker.setRefId(addArcToDb(arcShapeMarker.toDbEntity(), marker.getName(), marker.getColor()));
                break;
            case LINE:
                LineShapeMarker lineShapeMarker = (LineShapeMarker) marker;
                marker.setRefId(addLineToDb(lineShapeMarker.toDbEntity(), marker.getName(), marker.getColor()));
                break;
            case CIRCLE:
                CircleShapeMarker circleShapeMarker = (CircleShapeMarker) marker;
                marker.setRefId(addCircleToDb(circleShapeMarker.toDbEntity(), marker.getName(), marker.getColor()));
                break;
        }
    }

    public void retriveAllShapes(final GetAllShapesObserver observer) {
        database.circleDoa().getAll().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Consumer<List<CircleShape>>() {
                    @Override
                    public void accept(List<CircleShape> circleShapes) throws Exception {
                        for (final CircleShape shape : circleShapes) {
                            database.shapeDoa().loadAllBySubIds(new int[]{shape.uid})
                                    .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread())
                                    .subscribe(new Consumer<List<Shape>>() {
                                        @Override
                                        public void accept(List<Shape> parentShapes) throws Exception {
                                            Shape parent = (parentShapes != null && parentShapes.size() > 0) ? parentShapes.get(0) : null;
                                            if (parent != null) {
                                                observer.circleShapeRetrieved(CircleShapeMarker.fromEntity(shape, parent.name, parent.uid));
                                            }
                                            else {
                                                observer.circleShapeRetrieved(CircleShapeMarker.fromEntity(shape, Constant.DEFAULT_NAME, -1));
                                            }
                                        }
                                    });
                        }
                    }
                });

        database.arcDoa().getAll().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Consumer<List<ArcShape>>() {
                    @Override
                    public void accept(List<ArcShape> arcShapes) throws Exception {
                        for (final ArcShape shape : arcShapes) {
                            database.shapeDoa().loadAllBySubIds(new int[]{shape.uid})
                                    .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread())
                                    .subscribe(new Consumer<List<Shape>>() {
                                        @Override
                                        public void accept(List<Shape> parentShapes) throws Exception {
                                            Shape parent = (parentShapes != null && parentShapes.size() > 0) ? parentShapes.get(0) : null;
                                            if (parent != null) {
                                                observer.arcShapeRetrieved(ArcShapeMarker.fromEntity(shape, parent.name, parent.uid));
                                            }
                                            else {
                                                observer.arcShapeRetrieved(ArcShapeMarker.fromEntity(shape, Constant.DEFAULT_NAME, -1));
                                            }
                                        }
                                    });
                        }
                    }
                });

        database.lineDoa().getAll().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Consumer<List<LineShape>>() {
                    @Override
                    public void accept(List<LineShape> lineShapes) throws Exception {
                        for (final LineShape shape : lineShapes) {
                            database.shapeDoa().loadAllBySubIds(new int[]{shape.uid})
                                    .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread())
                                    .subscribe(new Consumer<List<Shape>>() {
                                        @Override
                                        public void accept(List<Shape> parentShapes) throws Exception {
                                            Shape parent = (parentShapes != null && parentShapes.size() > 0) ? parentShapes.get(0) : null;
                                            if (parent != null) {
                                                observer.lineShapeRetrieved(LineShapeMarker.fromEntity(shape, parent.name, parent.uid));
                                            }
                                            else {
                                                observer.lineShapeRetrieved(LineShapeMarker.fromEntity(shape, Constant.DEFAULT_NAME, -1));
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    public void deleteArc(ArcShapeMarker marker) {
        database.arcDoa().delete(marker.toDbEntity());
        database.shapeDoa().delete(marker.getRefId(), Constant.ARC_SHAPE);
    }

    public void deleteLine(LineShapeMarker marker) {
        database.lineDoa().delete(marker.toDbEntity());
        database.shapeDoa().delete(marker.getRefId(), Constant.LINE_SHAPE);
    }

    public void deleteCircle(CircleShapeMarker marker) {
        database.circleDoa().delete(marker.toDbEntity());
        database.shapeDoa().delete(marker.getRefId(), Constant.CIRCLE_SHAPE);
    }
}
