package com.example.rahulverlekar.smaplemapmarking.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rahulverlekar.smaplemapmarking.R;
import com.example.rahulverlekar.smaplemapmarking.helper.GoogleMapHelper;
import com.example.rahulverlekar.smaplemapmarking.markers.ArcShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.CircleShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.LineShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.ShapeMarker;
import com.google.maps.android.SphericalUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rahulverlekar on 26/03/18.
 */

public class ShapesAdapter extends RecyclerView.Adapter<ShapesAdapter.ShapeViewHolder> {

    private GoogleMapHelper helper;

    public ShapesAdapter(GoogleMapHelper helper) {
        this.helper = helper;
    }

    @NonNull
    @Override
    public ShapeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ShapeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.shape_item_view, parent, false), helper);
    }

    @Override
    public void onBindViewHolder(@NonNull ShapeViewHolder holder, int position) {
        ShapeMarker marker = helper.getElementAtPosition(position);
        if (marker != null) {
            holder.bindData(marker);
        }
    }

    @Override
    public int getItemCount() {
        return helper.getMarkerCount();
    }

    static class ShapeViewHolder extends RecyclerView.ViewHolder {
        private final GoogleMapHelper helper;
        @BindView(R.id.txt_shape_name)
        TextView txtShapeName;
        @BindView(R.id.txt_time_added)
        TextView txtTimeAdded;
        @BindView(R.id.txt_shape_type)
        TextView txtShapeType;
        @BindView(R.id.txt_shape_properties)
        TextView txtShapeProperties;

        ShapeViewHolder(View view, GoogleMapHelper helper) {
            super(view);
            this.helper = helper;
            ButterKnife.bind(this, view);
        }

        public void bindData(ShapeMarker marker) {
            itemView.setTag(marker);
            txtShapeName.setText(marker.getName());
            switch (marker.getType()) {
                case CIRCLE:
                    txtShapeType.setText(R.string.circle_marker);
                    txtShapeProperties.setText("Radius:" + ((CircleShapeMarker)marker).getRadius());
                    break;
                case LINE:
                    txtShapeType.setText(R.string.line_marker);
                    LineShapeMarker lineShapeMarker = (LineShapeMarker)marker;
                    double length = SphericalUtil.computeDistanceBetween(lineShapeMarker.getStartLatLng(), lineShapeMarker.getEndLatLong());
                    txtShapeProperties.setText(String.format("Length: %2.4f Meters", length));
                    break;
                case ARC:
                    txtShapeType.setText(R.string.arc_marker);
                    ArcShapeMarker arcShapeMarker = (ArcShapeMarker) marker;
                    txtShapeProperties.setText("Radius:" + arcShapeMarker.getRadius() + ",Width:" + arcShapeMarker.getWidth());
                    break;
            }
        }

        @OnClick(R.id.parent_layout)
        public void onClick(View v) {
            helper.shapeClicked((ShapeMarker)itemView.getTag());
        }

        @OnClick(R.id.iv_delete)
        public void onDeleteClick(View v) {
            helper.deleteClicked((ShapeMarker)itemView.getTag());
        }
    }
}
