package com.example.rahulverlekar.smaplemapmarking.dagger;

import com.example.rahulverlekar.smaplemapmarking.HomeActivity;
import com.example.rahulverlekar.smaplemapmarking.helper.GoogleMapHelper;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { DaggerModule.class})
public interface DaggerComponent {

    void inject(HomeActivity activity);
}