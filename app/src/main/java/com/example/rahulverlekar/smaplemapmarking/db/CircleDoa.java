package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by rahulverlekar on 25/03/18.
 */
@Dao
public interface CircleDoa {
    @Query("SELECT * FROM circle_shape")
    Flowable<List<CircleShape>> getAll();

    @Query("SELECT * FROM circle_shape WHERE uid IN (:ids)")
    LiveData<List<CircleShape>> loadAllByIds(int[] ids);

    @Insert
    List<Long> insertAll(CircleShape... shapes);

    @Delete
    void delete(CircleShape circleShape);
}
