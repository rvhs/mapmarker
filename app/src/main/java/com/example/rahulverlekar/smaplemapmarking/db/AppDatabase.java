package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

/**
 * Created by rahulverlekar on 25/03/18.
 */

@Database(entities = {CircleShape.class, LineShape.class, ArcShape.class, Shape.class}, version = 2, exportSchema = false)
@TypeConverters(Converters.class)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ShapeDoa shapeDoa();

    public abstract CircleDoa circleDoa();

    public abstract LineDoa lineDoa();

    public abstract ArcDoa arcDoa();
}
