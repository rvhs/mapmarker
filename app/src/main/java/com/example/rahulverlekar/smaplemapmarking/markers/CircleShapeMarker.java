package com.example.rahulverlekar.smaplemapmarking.markers;

import com.example.rahulverlekar.smaplemapmarking.db.CircleShape;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by rahulverlekar on 21/03/18.
 */

public class CircleShapeMarker extends ShapeMarker {

    private double radius;

    private LatLng latLng;

    public CircleShapeMarker(double radius, LatLng latLng, String name, int id) {
        super(ShapeMarkerType.CIRCLE, name, id);
        this.radius = radius;
        this.latLng = latLng;
    }

    public double getRadius() {
        return radius;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public CircleShape toDbEntity() {
        CircleShape shape = new CircleShape();
        shape.latitude = latLng.latitude;
        shape.longitude = latLng.longitude;
        shape.radius = radius;
        return shape;
    }

    public static CircleShapeMarker fromEntity(CircleShape shape, String name, int id) {
        return new CircleShapeMarker(shape.radius, new LatLng(shape.latitude, shape.longitude), name, id);
    }
}
