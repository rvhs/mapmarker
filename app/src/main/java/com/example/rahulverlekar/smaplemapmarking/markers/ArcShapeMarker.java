package com.example.rahulverlekar.smaplemapmarking.markers;

import com.example.rahulverlekar.smaplemapmarking.db.ArcShape;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by rahulverlekar on 21/03/18.
 */

public class ArcShapeMarker extends ShapeMarker {

    private LatLng latLng;
    private Double width;
    private Double radius;

    public ArcShapeMarker(LatLng latLng, Double width, Double radius, String name, int refid) {
        super(ShapeMarkerType.ARC, name, refid);
        this.latLng = latLng;
        this.width = width;
        this.radius = radius;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public Double getWidth() {
        return width;
    }

    public Double getRadius() {
        return radius;
    }

    public ArcShape toDbEntity() {
        ArcShape shape = new ArcShape();
        shape.latitude = latLng.latitude;
        shape.longitude = latLng.longitude;
        shape.radius = radius;
        shape.width = width;
        return shape;
    }

    public static ArcShapeMarker fromEntity(ArcShape shape, String name, int id) {
        return new ArcShapeMarker(new LatLng(shape.latitude, shape.longitude), shape.width, shape.radius, name, id);
    }
}
