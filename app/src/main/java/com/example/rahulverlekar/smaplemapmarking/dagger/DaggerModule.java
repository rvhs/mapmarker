package com.example.rahulverlekar.smaplemapmarking.dagger;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.rahulverlekar.smaplemapmarking.Constant;
import com.example.rahulverlekar.smaplemapmarking.db.AppDatabase;
import com.example.rahulverlekar.smaplemapmarking.helper.GoogleMapHelper;
import com.example.rahulverlekar.smaplemapmarking.helper.RoomDbHelper;
import com.google.android.gms.maps.GoogleMap;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by rahulverlekar on 26/03/18.
 */
@Module
public class DaggerModule {

    private Context context;
    private GoogleMap map;

    public DaggerModule(Context context, GoogleMap map) {
        this.context = context;
        this.map = map;
    }

    @Provides
    @Singleton
    public AppDatabase provideAppDatabase() {
        return Room.databaseBuilder(context, AppDatabase.class, Constant.DATABASE_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    public RoomDbHelper provideRoomDbHelper(AppDatabase database) {
        return new RoomDbHelper(context, database);
    }

    @Provides
    public GoogleMapHelper provideGoogleMapHelper() {
        return new GoogleMapHelper(map);
    }



}
