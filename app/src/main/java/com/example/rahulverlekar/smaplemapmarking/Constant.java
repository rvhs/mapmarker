package com.example.rahulverlekar.smaplemapmarking;

import android.graphics.Color;

/**
 * Created by rahulverlekar on 25/03/18.
 */

public class Constant {
    public static final int CIRCLE_SHAPE = 1;
    public static final int LINE_SHAPE = 2;
    public static final int ARC_SHAPE = 3;
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_COLOR = "";
    public static final String DATABASE_NAME = "shape_db";

    public static final int DEFAULT_ARC_COLOR = Color.GRAY;
    public static final int DEFAULT_CIRCLE_STROKE_COLOR = Color.GRAY;
    public static final int DEFAULT_CIRCLE_FILL_COLOR = Color.GRAY;
    public static final int DEFAULT_LINE_COLOR = Color.GRAY;
}
