package com.example.rahulverlekar.smaplemapmarking.markers;

import com.example.rahulverlekar.smaplemapmarking.Constant;

/**
 * Created by rahulverlekar on 21/03/18.
 */

//This is the base class of all the markers we are showing
public class ShapeMarker {

    private ShapeMarkerType type;
    private String name;
    private String color;
    private int refId;

    public ShapeMarker(ShapeMarkerType type, String name, int refId) {
        this.type = type;
        this.name = name;
        this.refId = refId;
        color = Constant.DEFAULT_COLOR;
    }

    public ShapeMarker(ShapeMarkerType type, String name, String color) {
        this.type = type;
        this.name = name;
        this.color = color;
    }

    public ShapeMarkerType getType() {
        return type;
    }

    public void setType(ShapeMarkerType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getRefId() {
        return refId;
    }

    public void setRefId(int refId) {
        this.refId = refId;
    }
}
