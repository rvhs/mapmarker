package com.example.rahulverlekar.smaplemapmarking;

import android.app.Dialog;
import android.support.constraint.Group;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rahulverlekar on 24/03/18.
 */

public class SelectSettingDialogViewHolder {
    @BindView(R.id.edt_shape_width)
    EditText edtShapeWidth;
    @BindView(R.id.edt_shape_length)
    EditText edtShapeLength;
    @BindView(R.id.edt_shape_radius)
    EditText edtShapeRadius;
    @BindView(R.id.group_radius)
    Group groupRadius;
    @BindView(R.id.group_length)
    Group groupLength;
    @BindView(R.id.group_width)
    Group groupWidth;

    private Dialog dialog;
    private SettingChangeListener listener;

    public SelectSettingDialogViewHolder(Dialog dialog, SettingChangeListener listener, View v) {
        this.listener = listener;
        ButterKnife.bind(this, v);
        this.dialog = dialog;
    }

    public void show() {
        dialog.show();
    }

    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {
        listener.onSettingChange(Double.parseDouble(edtShapeRadius.getText().toString()) ,
                Double.parseDouble(edtShapeLength.getText().toString()),
                Double.parseDouble(edtShapeWidth.getText().toString()), "");
        dialog.dismiss();
    }

    public interface SettingChangeListener {

        void onSettingChange(double radius, double length, double width, String name);

        void onDismiss();
    }
}
