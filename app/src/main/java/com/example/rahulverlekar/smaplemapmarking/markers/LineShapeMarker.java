package com.example.rahulverlekar.smaplemapmarking.markers;

import com.example.rahulverlekar.smaplemapmarking.db.LineShape;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by rahulverlekar on 21/03/18.
 */

public class LineShapeMarker extends ShapeMarker {

    private LatLng startLatLng, endLatLong;
    private double length;
    private double width;

    public LineShapeMarker(LatLng startLatLng, LatLng endLatLong, double length, double width, String name, int id) {
        super(ShapeMarkerType.LINE, name, id);
        this.startLatLng = startLatLng;
        this.endLatLong = endLatLong;
        this.length = length;
        this.width = width;
    }

    public LineShape toDbEntity() {
        LineShape shape = new LineShape();
        shape.srcLatitude = startLatLng.latitude;
        shape.srcLongitude = startLatLng.longitude;
        shape.destLatitude = endLatLong.latitude;
        shape.destLongitude = endLatLong.longitude;
        shape.width = width;
        return shape;
    }

    public static LineShapeMarker fromEntity(LineShape shape, String name, int id) {
        return new LineShapeMarker(new LatLng(shape.srcLatitude, shape.srcLongitude),
                new LatLng(shape.destLatitude, shape.destLongitude), 0, shape.width, name, id);
    }

    public LatLng getStartLatLng() {
        return startLatLng;
    }

    public LatLng getEndLatLong() {
        return endLatLong;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }
}
