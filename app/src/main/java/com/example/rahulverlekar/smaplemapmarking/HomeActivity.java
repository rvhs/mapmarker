package com.example.rahulverlekar.smaplemapmarking;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.rahulverlekar.smaplemapmarking.adapter.ShapesAdapter;
import com.example.rahulverlekar.smaplemapmarking.dagger.DaggerComponent;
import com.example.rahulverlekar.smaplemapmarking.dagger.DaggerDaggerComponent;
import com.example.rahulverlekar.smaplemapmarking.dagger.DaggerModule;
import com.example.rahulverlekar.smaplemapmarking.db.AppDatabase;
import com.example.rahulverlekar.smaplemapmarking.helper.GoogleMapHelper;
import com.example.rahulverlekar.smaplemapmarking.helper.MapInteractions;
import com.example.rahulverlekar.smaplemapmarking.helper.RoomDbHelper;
import com.example.rahulverlekar.smaplemapmarking.markers.ArcShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.CircleShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.LineShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.ShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.ShapeMarkerType;
import com.example.rahulverlekar.smaplemapmarking.observer.GetAllShapesObserver;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        MapInteractions,
        SelectSettingDialogViewHolder.SettingChangeListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_shapes)
    RecyclerView rvShapes;

    private SelectionOptions currentSelection;
    private MapStateMachine currentState;
    private SelectSettingDialogViewHolder dialog;
    private ShapesAdapter shapesAdapter = null;

    @Inject
    public AppDatabase database;

    @Inject
    public RoomDbHelper dbHelper;

    @Inject
    public GoogleMapHelper mapHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        currentSelection = new SelectionOptions();
        currentState = MapStateMachine.NO_MARKER_SET;
        setSupportActionBar(toolbar);
        initSettingDailog();
    }

    private void initDIcomponent(GoogleMap googleMap) {
        DaggerComponent component = DaggerDaggerComponent.builder().
                daggerModule(new DaggerModule(this, googleMap)).build();
        component.inject(this);
    }

    @Override
    public void onCircleShapeClicked(CircleShapeMarker shape) {
        int pos = mapHelper.getCircleAdapterPosition(shape.getRefId());
        if (pos != -1) {
            rvShapes.smoothScrollToPosition(pos);
        }
    }

    @Override
    public void onLineShapeClicked(LineShapeMarker shape) {
        int pos = mapHelper.getLineAdapterPosition(shape.getRefId());
        if (pos != -1) {
            rvShapes.smoothScrollToPosition(pos);
        }
    }

    @Override
    public void onArcShapeClicked(ArcShapeMarker shape) {
        int pos = mapHelper.getArcAdapterPosition(shape.getRefId());
        if (pos != -1) {
            rvShapes.smoothScrollToPosition(pos);
        }
    }

    @OnClick(R.id.toggle_show_marker)
    public void toggleShowShapes(View view) {
        if (shapesAdapter == null) {
            shapesAdapter = new ShapesAdapter(mapHelper);
            rvShapes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvShapes.setAdapter(shapesAdapter);
        }
        if (rvShapes.getVisibility() == View.GONE) {
            rvShapes.setVisibility(View.VISIBLE);
        }
        else {
            rvShapes.setVisibility(View.GONE);
        }
        shapesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLongPress(LatLng latLng) {
        switch (currentSelection.getSelectedShape()) {
            case ARC:
                currentSelection.setSource(latLng);
                currentState = MapStateMachine.SOURCE_LATLNG_SET;
                ArcShapeMarker arcShapeMarker = new ArcShapeMarker(latLng, currentSelection.getWidth(), currentSelection.getRadius(), Constant.DEFAULT_NAME, -1);
                mapHelper.drawArch(arcShapeMarker);
                getNameAndSaveShapeData(arcShapeMarker);
                mapHelper.removeMarkersFromMap();
                clearSelectionPallet();
                break;
            case CIRCLE:
                currentSelection.setSource(latLng);
                currentState = MapStateMachine.SOURCE_LATLNG_SET;
                CircleShapeMarker circleShapeMarker = new CircleShapeMarker(currentSelection.getRadius(), latLng, Constant.DEFAULT_NAME, -1);
                mapHelper.drawCircle(circleShapeMarker);
                getNameAndSaveShapeData(circleShapeMarker);
                mapHelper.removeMarkersFromMap();
                clearSelectionPallet();
                break;
            case LINE:
                if (currentState.equals(MapStateMachine.SOURCE_LATLNG_SET)) {
                    currentSelection.setDestination(latLng);
                    currentState = MapStateMachine.DESTINATION_LATLNG_MARKER_SET;
                    LineShapeMarker lineShapeMarker = new LineShapeMarker(currentSelection.getSource(), currentSelection.getDestination(), currentSelection.getLength(), currentSelection.getWidth(), Constant.DEFAULT_NAME, -1);
                    mapHelper.drawLine(lineShapeMarker);
                    getNameAndSaveShapeData(lineShapeMarker);
                    mapHelper.removeMarkersFromMap();
                    clearSelectionPallet();
                } else {
                    currentSelection.setSource(latLng);
                    currentState = MapStateMachine.SOURCE_LATLNG_SET;
                    mapHelper.addSourceMarker(latLng);
                }
                break;
        }
    }

    @Override
    public void deleteArc(ArcShapeMarker marker) {
        dbHelper.deleteArc(marker);
        shapesAdapter.notifyDataSetChanged();
    }

    @Override
    public void deleteLine(LineShapeMarker marker) {
        dbHelper.deleteLine(marker);
        shapesAdapter.notifyDataSetChanged();
    }

    @Override
    public void deleteCircle(CircleShapeMarker marker) {
        dbHelper.deleteCircle(marker);
        shapesAdapter.notifyDataSetChanged();
    }

    public class GetExistingShapesObserver implements GetAllShapesObserver {

        @Override
        public void circleShapeRetrieved(CircleShapeMarker shape) {
            mapHelper.drawCircle(shape);
        }

        @Override
        public void lineShapeRetrieved(LineShapeMarker shape) {
            mapHelper.drawLine(shape);
        }

        @Override
        public void arcShapeRetrieved(ArcShapeMarker shape) {
            mapHelper.drawArch(shape);
        }
    }

    private void initSettingDailog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View layout = getLayoutInflater().inflate(R.layout.shape_prefrence_view, null);
        builder.setView(layout);
        dialog = new SelectSettingDialogViewHolder(builder.create(), this, layout);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        initDIcomponent(googleMap);
        mapHelper.onMapReady(this);
        dbHelper.retriveAllShapes(new GetExistingShapesObserver());
    }


    private void clearSelectionPallet() {
        currentSelection.clearToDefaultState();
        currentState = MapStateMachine.NO_MARKER_SET;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_arc:
                currentSelection.setSelectedShape(ShapeMarkerType.ARC);
                return true;
            case R.id.action_circle:
                currentSelection.setSelectedShape(ShapeMarkerType.CIRCLE);
                return true;
            case R.id.action_line:
                currentSelection.setSelectedShape(ShapeMarkerType.LINE);
                return true;
            case R.id.action_setting:
                showShapeSetting();
                return true;
            default:
                currentSelection.setSelectedShape(ShapeMarkerType.CIRCLE);
                return super.onOptionsItemSelected(item);
        }
    }

    void showShapeSetting() {
        dialog.show();
    }

    @Override
    public void onSettingChange(double radius, double length, double width, String name) {
        currentSelection.setRadius(radius);
        currentSelection.setLength(length);
        currentSelection.setWidth(width);
        currentSelection.setName(name);
    }

    @Override
    public void onDismiss() {

    }

    private void getNameAndSaveShapeData(final ShapeMarker marker) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter the label");

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = input.getText().toString();
                marker.setName(name);
                dbHelper.addShapeToDb(marker);
                mapHelper.drawShape(marker);
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dbHelper.addShapeToDb(marker);
                mapHelper.drawShape(marker);
            }
        });

        builder.show();
    }
}
