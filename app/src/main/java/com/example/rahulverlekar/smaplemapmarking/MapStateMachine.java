package com.example.rahulverlekar.smaplemapmarking;

/**
 * Created by rahulverlekar on 22/03/18.
 */

public enum MapStateMachine {
    NO_MARKER_SET, SOURCE_LATLNG_SET, DESTINATION_LATLNG_MARKER_SET, EDIT_MARKER
}
