package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by rahulverlekar on 25/03/18.
 */
@Dao
public interface ShapeDoa {
    @Query("SELECT * FROM shape")
    Flowable<List<Shape>> getAll();

    @Query("SELECT * FROM shape WHERE uid IN (:ids)")
    LiveData<List<Shape>> loadAllByIds(int[] ids);

    @Query("SELECT * FROM shape WHERE sub_id IN (:ids)")
    Flowable<List<Shape>> loadAllBySubIds(int[] ids);

    @Insert
    List<Long> insertAll(Shape... shapes);

    @Delete
    void delete(Shape shape);

    @Query("DELETE FROM shape WHERE sub_id = :refId AND type = :type")
    void delete(int refId, int type);
}
