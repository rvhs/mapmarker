package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class Converters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static Double fromText(String value) {
        return value == null ? 0 : Double.parseDouble(value);
    }

    @TypeConverter
    public static String doubleToString(Double value) {
        return value == null ? "0" : value.toString();
    }
}