package com.example.rahulverlekar.smaplemapmarking.markers;

/**
 * Created by rahulverlekar on 21/03/18.
 */

public enum ShapeMarkerType {
    ARC, CIRCLE, LINE
}
