package com.example.rahulverlekar.smaplemapmarking.helper;

import com.example.rahulverlekar.smaplemapmarking.markers.ArcShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.CircleShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.LineShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.ShapeMarker;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by rahulverlekar on 26/03/18.
 */

public interface MapInteractions {

    void onCircleShapeClicked(CircleShapeMarker shape);

    void onLineShapeClicked(LineShapeMarker shape);

    void onArcShapeClicked(ArcShapeMarker shape);

    void onLongPress(LatLng latLng);

    void deleteArc(ArcShapeMarker marker);

    void deleteLine(LineShapeMarker marker);

    void deleteCircle(CircleShapeMarker marker);
}
