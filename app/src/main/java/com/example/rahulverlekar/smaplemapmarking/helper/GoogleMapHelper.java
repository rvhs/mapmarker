package com.example.rahulverlekar.smaplemapmarking.helper;

import android.graphics.Color;
import android.os.Handler;
import android.util.SparseArray;

import com.example.rahulverlekar.smaplemapmarking.Constant;
import com.example.rahulverlekar.smaplemapmarking.db.CircleShape;
import com.example.rahulverlekar.smaplemapmarking.markers.ArcShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.CircleShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.LineShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.ShapeMarker;
import com.example.rahulverlekar.smaplemapmarking.markers.ShapeMarkerType;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.ButtCap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by rahulverlekar on 26/03/18.
 */

public class GoogleMapHelper implements GoogleMap.OnCircleClickListener, GoogleMap.OnPolylineClickListener, GoogleMap.OnMapLongClickListener {

    private GoogleMap map;

    private Marker sourceMarker;
    private MapInteractions listner;

    private SparseArray<Circle> mapCircles;
    private SparseArray<Polyline> mapArcs;
    private SparseArray<Polyline> mapLines;

    public GoogleMapHelper(GoogleMap map) {
        this.map = map;
        map.setOnPolylineClickListener(this);
        map.setOnCircleClickListener(this);
        mapCircles = new SparseArray<>();
        mapArcs = new SparseArray<>();
        mapLines = new SparseArray<>();
    }

    public void drawCircle(CircleShapeMarker marker) {
        Circle circle = map.addCircle(new CircleOptions()
                .center(marker.getLatLng())
                .radius(marker.getRadius())
                .strokeColor(Constant.DEFAULT_CIRCLE_STROKE_COLOR)
                .fillColor(Constant.DEFAULT_CIRCLE_FILL_COLOR));
        circle.setTag(marker);
        circle.setClickable(true);
        mapCircles.put(marker.getRefId(), circle);
    }


    public void drawLine(LineShapeMarker lineShapeMarker) {
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.add(lineShapeMarker.getStartLatLng())
                .add(lineShapeMarker.getEndLatLong())
                .color(Constant.DEFAULT_LINE_COLOR);
        Polyline polyline = map.addPolyline(polylineOptions);
        polyline.setTag(lineShapeMarker);
        polyline.setClickable(true);
        mapLines.put(lineShapeMarker.getRefId(), polyline);
    }

    public void drawArch(ArcShapeMarker marker) {
        double radius = marker.getRadius() * 0.0000139;
        LatLng center = marker.getLatLng();
        double k = 1;
        LatLng p1 = new LatLng(center.latitude, center.longitude + radius);
        LatLng p2 = new LatLng(center.latitude, center.longitude - radius);
        //Calculate distance and heading between two points
        double d = SphericalUtil.computeDistanceBetween(p1, p2);
        double h = SphericalUtil.computeHeading(p1, p2);

        //Midpoint position
        LatLng p = SphericalUtil.computeOffset(p1, d * 0.5, h);

        //Apply some mathematics to calculate position of the circle center
        double x = (1 - k * k) * d * 0.5 / (2 * k);
        double r = (1 + k * k) * d * 0.5 / (2 * k);

        LatLng c = SphericalUtil.computeOffset(p, x, h + 90.0);

        //Polyline options
        PolylineOptions options = new PolylineOptions();
        options.endCap(new ButtCap());

        //Calculate heading between circle center and two points
        double h1 = SphericalUtil.computeHeading(c, p1);
        double h2 = SphericalUtil.computeHeading(c, p2);

        //Calculate positions of points on circle border and add them to polyline options
        int numpoints = 100;
        double step = (h2 - h1) / numpoints;

        for (int i = 0; i < numpoints; i++) {
            LatLng pi = SphericalUtil.computeOffset(c, r, h1 + i * step);
            options.add(pi);
        }

        //Draw polyline
        Polyline polyline = map.addPolyline(options.width(marker.getWidth().floatValue()).color(Constant.DEFAULT_ARC_COLOR).geodesic(false));
        polyline.setClickable(true);
        polyline.setTag(marker);
        mapArcs.put(marker.getRefId(), polyline);
    }

    public void removeMarkersFromMap() {
        if (sourceMarker != null) {
            sourceMarker.remove();
        }
    }

    public void addSourceMarker(LatLng source) {
        sourceMarker = map.addMarker(new MarkerOptions().position(source).title("Source"));
    }

    @Override
    public void onCircleClick(Circle circle) {
        Object tag = circle.getTag();
        if (tag != null && tag instanceof CircleShapeMarker) {
            if (listner != null) {
                listner.onCircleShapeClicked((CircleShapeMarker) tag);
            }
        }
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        Object tag = polyline.getTag();
        if (tag != null && tag instanceof ShapeMarker) {
            ShapeMarker marker = (ShapeMarker) tag;
            if (marker.getType() == ShapeMarkerType.ARC) {
                if (listner != null) {
                    listner.onArcShapeClicked((ArcShapeMarker) marker);
                }
            }
            else if (marker.getType() == ShapeMarkerType.LINE) {
                if (listner != null) {
                    listner.onLineShapeClicked((LineShapeMarker) marker);
                }
            }
        }
    }

    public void onMapReady(MapInteractions listner) {
        this.listner = listner;
        LatLng coordinate = new LatLng(15.50092811612795,73.82725428789854);
        moveCameraToLatlng(coordinate);
        map.setOnMapLongClickListener(this);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if (listner != null) {
            listner.onLongPress(latLng);
        }
    }

    public int getMarkerCount() {
        return mapArcs.size() + mapCircles.size() + mapLines.size();
    }

    public ShapeMarker getElementAtPosition(int pos) {
        if (pos >= 0 && pos < mapCircles.size()) {
            return (ShapeMarker) mapCircles.get(mapCircles.keyAt(pos)).getTag();
        }
        if (pos >= mapCircles.size() && pos < mapArcs.size() + mapCircles.size()) {
            pos = pos - mapCircles.size();
            return (ShapeMarker) mapArcs.get(mapArcs.keyAt(pos)).getTag();
        }
        if (pos >= mapArcs.size() + mapCircles.size() && pos < mapArcs.size() + mapCircles.size() + mapLines.size()) {
            pos = pos - mapCircles.size() - mapArcs.size();
            return (ShapeMarker) mapLines.get(mapLines.keyAt(pos)).getTag();
        }
        return null;
    }

    public void shapeClicked(ShapeMarker marker) {
        final Handler handler = new Handler();
        switch (marker.getType()) {
            case ARC:
                moveCameraToLatlng(((ArcShapeMarker)marker).getLatLng());
                final Polyline polyline = mapArcs.get(marker.getRefId());
                polyline.setColor(Color.BLACK);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        polyline.setColor(Constant.DEFAULT_ARC_COLOR);
                    }
                }, 500);
                break;
            case LINE:
                moveCameraToLatlng(((LineShapeMarker)marker).getStartLatLng());
                final Polyline line = mapLines.get(marker.getRefId());
                line.setColor(Color.BLACK);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        line.setColor(Constant.DEFAULT_LINE_COLOR);
                    }
                }, 500);
                break;
            case CIRCLE:
                moveCameraToLatlng(((CircleShapeMarker)marker).getLatLng());
                final Circle circle = mapCircles.get(marker.getRefId());
                circle.setFillColor(Color.BLACK);
                circle.setStrokeColor(Color.BLACK);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        circle.setFillColor(Constant.DEFAULT_CIRCLE_FILL_COLOR);
                        circle.setStrokeColor(Constant.DEFAULT_CIRCLE_STROKE_COLOR);
                    }
                }, 500);
                break;
        }
    }

    private void moveCameraToLatlng(LatLng latLng) {
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                latLng, 15);
        map.animateCamera(location);
    }


    public void drawShape(ShapeMarker marker) {
        switch (marker.getType()) {
            case CIRCLE:
                drawCircle((CircleShapeMarker) marker);
                break;
            case LINE:
                drawLine((LineShapeMarker) marker);
                break;
            case ARC:
                drawArch((ArcShapeMarker) marker);
                break;
        }
    }

    public int getCircleAdapterPosition(int refId) {
        int pos = mapCircles.indexOfKey(refId);
        if(pos > 0) {
            return pos;
        }
        return -1;
    }

    public int getArcAdapterPosition(int refId) {
        int pos = mapArcs.indexOfKey(refId);
        if(pos > 0) {
            return pos + mapCircles.size();
        }
        return -1;
    }

    public int getLineAdapterPosition(int refId) {
        int pos = mapLines.indexOfKey(refId);
        if(pos > 0) {
            return pos + mapCircles.size() + mapArcs.size();
        }
        return -1;
    }

    public void deleteClicked(ShapeMarker marker) {
        switch (marker.getType()) {
            case CIRCLE:
                mapCircles.get(marker.getRefId()).remove();
                mapCircles.remove(marker.getRefId());
                listner.deleteCircle((CircleShapeMarker) marker);
                break;
            case LINE:
                mapLines.get(marker.getRefId()).remove();
                mapLines.remove(marker.getRefId());
                listner.deleteLine((LineShapeMarker) marker);
                break;
            case ARC:
                mapArcs.get(marker.getRefId()).remove();
                mapArcs.remove(marker.getRefId());
                listner.deleteArc((ArcShapeMarker) marker);
                break;
        }
    }
}
