package com.example.rahulverlekar.smaplemapmarking.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by rahulverlekar on 25/03/18.
 */

@Entity
public class Shape {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "type")
    public int type;

    @ColumnInfo(name = "color")
    public String color;

    @ColumnInfo(name = "sub_id")
    public int subid;

}
